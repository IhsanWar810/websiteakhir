document.addEventListener("DOMContentLoaded", function () {
    const toggleButton = document.getElementById("b-button");
    const menu = document.getElementById("menu");

    toggleButton.addEventListener("click", function () {
        if (menu.style.display === "block") {
            menu.style.display = "none";
        } else {
            menu.style.display = "block";
        }
    })
    const background = document.querySelector('.background');

    document.addEventListener('mousemove', (e) => {
        const mouseX = e.clientX / window.innerWidth;
        const mouseY = e.clientY / window.innerHeight;
        
        background.style.opacity = 1 - (mouseX + mouseY) / 2;
    })
});